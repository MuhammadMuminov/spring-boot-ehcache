package ai.ecma.springcaching.repository;

import ai.ecma.springcaching.entity.Country;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Author: Muhammad Mo'minov
 * 08.06.2021
 */
public interface CountryRepository extends JpaRepository<Country, Integer> {

}
