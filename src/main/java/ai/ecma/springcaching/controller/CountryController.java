package ai.ecma.springcaching.controller;

import ai.ecma.springcaching.entity.Country;
import ai.ecma.springcaching.model.ApiResponse;
import ai.ecma.springcaching.service.CountryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Muhammad Mo'minov
 * 08.06.2021
 */
@RestController
@RequestMapping("/api/country")
@AllArgsConstructor
public class CountryController {
    final CountryService countryService;

    @GetMapping("/{id}")
    public HttpEntity<?> getOne(@PathVariable int id){
        ApiResponse response = countryService.getOne(id);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    public HttpEntity<?> edit(@PathVariable int id, @RequestBody Country country){
        return ResponseEntity.ok(countryService.update(id, country));
    }

    @DeleteMapping("/{id}")
    public HttpEntity<?> deleteOne(@PathVariable int id){
        return ResponseEntity.ok(countryService.deleteOne(id));
    }

    @DeleteMapping("/all")
    public HttpEntity<?> deleteAll(){
        return ResponseEntity.ok(countryService.deleteAll());
    }
}
